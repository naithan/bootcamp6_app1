//
//  TableViewCell.h
//  Bootcamp6_app1
//
//  Created by Jeremi Kaczmarczyk on 20.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
